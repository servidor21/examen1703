<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 100; $i++) {
            $book = factory(App\Book::class)->create();
            $authors = App\Author::all()->random(rand(2, 4));

            $authors = DB::table('authors')->where('birth_year', '<', $book->year)->get()->random(rand(2, 4));

            // dd($authors);
            $count = 0;
            foreach ($authors as $author) {
                if ($count > 0) {
                    $book->authors()->attach($author->id);
                }
                $count++;
            }
        }
    }
}
