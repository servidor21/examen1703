<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genders')->insert([
            'name' => 'comedia',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ]);
        DB::table('genders')->insert([
            'name' => 'drama',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ]);
        DB::table('genders')->insert([
            'name' => 'tragedia',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ]);
        DB::table('genders')->insert([
            'name' => 'infantil',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ]);
        DB::table('genders')->insert([
            'name' => 'juvenil',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ]);
        DB::table('genders')->insert([
            'name' => 'poesía',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ]);
    }
}
