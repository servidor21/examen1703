@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Detalle de {{$book->name}}</p>
    </div>
    <p><b>ID:</b> {{ $book->id }}</p>
    <p><b>Título:</b> {{ $book->title }}</p>
    <p><b>Tipo:</b> {{ $book->gender->name }}</p>
    <p><b>Usuario:</b> {{ $book->user->name }}</p>
    <p><b>Autores:</b></p>
    <table class="table"> 
        <tr>
            <th>Nombre</th><th>País</th><th>Año</th>
        </tr>
        @foreach($book->authors as $author)
        <tr>
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $author->name }}</td>
            <td>{{ $author->country }}</td>
            <td>{{ $author->birth_year }}</td>
        </tr>
        @endforeach
        </table>

    @stop