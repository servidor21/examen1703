@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Lista de Libros</p>
    </div>
    @can('create', $books)
        <a href="/books/create" >Nuevo libro</a>
        @endcan
        <table class="table"> 
        <tr>
            <th>Usuario</th><th>Género del libro</th><th>Nombre</th><th>Páginas</th><th>Año de publicación</th>
        </tr>
        @foreach($books as $book)
        <tr>
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $book->user->name }}</td>
            <td>{{ $book->gender->name }}</td>
            <td>{{ $book->title }}</td>
            <td>{{ $book->pages }}</td>
            <td>{{ $book->year }}</td>
            <td>
            @can('delete', $book)
                <form method="post" action="/books/{{ $book->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                
                <input type="submit" value="Borrar">
            
            
                <a href="/books/{{ $book->id }}/edit">Editar</a>
            @endcan
                <a href="/books/{{ $book->id }}">Ver</a>
                </form>
            </td>
        </tr>
        @endforeach
        </table>
        @can('create', $book)
        <a href="/books/create" >Nuevo libro</a>
        @endcan

    <div class="pagination text-center">{!! $books->links() !!}</div>

    @stop