@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Alta de Libro</p>
    </div>
        
    <form action="/books" method="POST">
    {{ csrf_field() }}
        <div class="form-group">
            <label>Title:</label><input type="text" name="name">
            {{ $errors->first('title') }}
        </div>
        <div class="form-group">
            <label>Pages:</label><textarea class="form-control" rows="5" name="pages" id="pages"></textarea>
            {{ $errors->first('pages') }}
        </div>
         <div class="form-group">
            <label>Year:</label><input type="text" name="name">
            {{ $errors->first('year') }}
        </div>
        <div class="form-group">
            <select name="gender">
                @foreach($genders as $gender)
                    <option value="{{ $gender->id }}">{{ $gender->name }}</option>
                @endforeach
            </select>
            {{ $errors->first('gender') }}
        </div>
        <div class="form-group">
            <input type="submit" name="Crear" value="Crear">
        </div>
    </form>

    @stop